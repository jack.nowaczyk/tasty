<?php
global $app;
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
use \Slim\Http\Request;
use \Slim\Http\Response;


$app->add(function (Request $request, Response $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));
        
        if($request->getMethod() == 'GET') {
            return $response->withRedirect((string)$uri, 301);
        }
        else {
            return $next($request->withUri($uri), $response);
        }
    }
    return $next($request, $response);
});


$app->add(function (Request $request, Response $response, callable $next) {

    /** @var \Slim\Route $route **/
    $route=$request->getAttribute('route');
    if(!$route){
        if(!@$this->settings['determineRouteBeforeAppMiddleware']){
            assert(is_object($route),"route attribute not defined, please set 'determineRouteBeforeAppMiddleware=true' App parameter - access attribute will have no effect");
        }
        return $next($request, $response);//proceed to 404 not found
    }
    $this->AppModel->hasRouteAccess($route->getName(),true);
    return $next($request, $response);
});


$app->add(function (Request $request, Response $response, callable $next) {
    $key = $request->getParam('key');
    if($key){
        /** @var \Tasty\CompanyAdminModel $model **/
        $model = $this->CompanyAdminModel;
        $userId=$model->checkSecureKey($key);
        $model->loginUser($userId);
    }
    return $next($request, $response);
});

