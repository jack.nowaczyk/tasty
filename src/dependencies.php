<?php
// DIC configuration
global $app;
$container = $app->getContainer();

//-------------------------------- [dependencies: factory methods] --------------------------------

// Zend_Acl
$container['acl'] = function ($c) {
    return require __DIR__ . '/../src/acl.php';
};
// view
$container['view'] = function ($c) {
    $settings = $c->get('settings')['view'];
    $view = new \Slim\Views\Twig($settings['template_path'], $settings);
    /** @var Slim\Http\Uri $uri **/
    $uri=$c->get('request')->getUri();
    // Instantiate and add Slim specific extension
    $view->addExtension(new \Slim\Views\TwigExtension($c->get('router'), $uri));
    $view->addExtension(new \Tasty\AppTwigExtension($c->get('AppModel')));
    $view->getEnvironment()->getExtension('Twig_Extension_Core')->setDateFormat('Y-m-d H:i', '%d days');
    return $view;
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};


$container['company'] = function ($c) {
    $path=$c->request->getUri()->getPath();
    if(!preg_match('#^/([^/]+)#', $path ,$res))throw new Exception('invalid URI - company name is missing');
    $companyName=$res[1];
    $company=new \Tasty\Company($c->get('settings')['tasty']);
    if($companyName!='admin')$company->setName($companyName);
    return $company;
};

$container['mailer'] = function ($c) {
    $mailer = new \PHPMailer\PHPMailer\PHPMailer (true);
    $mailer->CharSet = "utf-8";
    $settings = @$c->get('settings')['mailer'];
    if($settings){
        $vars=get_class_vars(get_class($mailer));
        foreach($settings as $name=>$value){
            if(!isset($vars[$name]))throw new Exception("illegal PHPMailer param '$name', valid are: ".implode(', ', array_keys($vars)),500);
            $mailer->set($name,$value);//this will allow setting protected vars, so above error handler is needed
        }
    }
    return $mailer;
};

$container['AppModel'] = function ($c) {
    return new \Tasty\AppModel($c,$c->get('settings')['tasty']); //app commons
};

$container['UserModel'] = function ($c) {
    $path=$c->request->getUri()->getPath();
    if(!preg_match('#.*?/user/([^/]+)#', $path ,$res))throw new Exception('invalid URI - user name is missing');
    $user=$res[1];
    return new \Tasty\UserModel($c->get('company'),$user);
};

$container['DeveloperModel'] = function ($c) {
    return new \Tasty\DeveloperModel($c->get('AppModel'));
};

$container['AdminModel'] = function ($c) {
    return new \Tasty\AdminModel($c->get('settings')['tasty'],$c->get('company'));
};

$container['CompanyAdminModel'] = function ($c) {
    return new \Tasty\CompanyAdminModel($c->get('AppModel'),$c->get('company'));
};

$container['DeveloperController'] = function ($c) {
    return new \Tasty\DeveloperController($c->get('AppModel'), $c->get('DeveloperModel'));
};

$container['FrontController'] = function ($c) {
    return new \Tasty\FrontController($c->get('AppModel'),$c->get('settings')['tasty']);
};

$container['UserController'] = function ($c) {
    return new \Tasty\UserController($c->get('AppModel'), $c->get('UserModel'));
};

$container['CompanyAdminController'] = function ($c) {
    return new \Tasty\CompanyAdminController($c->get('AppModel'), $c->get('CompanyAdminModel'));
};

$container['AdminController'] = function ($c) {
    return new \Tasty\AdminController($c->get('AppModel'), $c->get('AdminModel'));
};