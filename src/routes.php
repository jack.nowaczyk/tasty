<?php

global $app;
$app->get('/developer', 'DeveloperController:home')->setName("developer")->setArgument('access', 'app:debug');
$app->get('/developer/demo/{params:.*}', 'DeveloperController:demo')->setName("developer/demo")->setArgument('access', 'app:debug');
$app->get('/developer/routes', 'DeveloperController:routes')->setName("developer/routes")->setArgument('access', 'app:debug');
$app->get('/developer/schemas', 'DeveloperController:schemas')->setName("developer/schemas")->setArgument('access', 'app:debug');
$app->get('/developer/editSchema/{name}', 'DeveloperController:editSchema')->setName("developer/editSchema")->setArgument('access', 'app:debug');
$app->post('/developer/editSchema/{name}', 'DeveloperController:editSchema')->setName("developer/editSchema")->setArgument('access', 'app:debug');

// Front routes
$app->get('/', 'FrontController:top')->setName("/");
$app->get('/join', 'FrontController:join')->setName("join");
$app->post('/join', 'FrontController:join');

//Cron routes:

// Admin routes
$app->any('/admin', 'AdminController:login')->setName("admin/login");
$app->any('/admin/logout', 'AdminController:logout')->setName("admin/logout");
$app->get('/admin/cron', 'AdminController:cron')->setName("admin/cron");

$app->get('/admin/home', 'AdminController:home')->setName("admin")->setArgument('access', 'app:admin');
$app->post('/admin/createCompany', 'AdminController:createCompany')->setName("admin/createCompany")->setArgument('access', 'app:admin');
$app->any('/admin/deleteCompany/{company}', 'AdminController:deleteCompany')->setName("admin/deleteCompany")->setArgument('access', 'app:admin');
$app->any('/admin/deleteMessage/{id}', 'AdminController:deleteMessage')->setName("admin/deleteMessage")->setArgument('access', 'app:admin');

$app->get('/{company}/admin/activate', 'CompanyAdminController:activate')->setName("company/admin/activate")->setArgument('access', 'app:admin');
$app->get('/{company}/admin/deleteMenu/{id}', 'CompanyAdminController:deleteMenu')->setName("company/admin/deleteMenu")->setArgument('access', 'menu:admin');
$app->get('/{company}/admin/editMenu/{id}', 'CompanyAdminController:editMenu')->setName("company/admin/editMenu")->setArgument('access', 'menu:admin');
$app->post('/{company}/admin/editMenu/{id}', 'CompanyAdminController:editMenu')->setArgument('access', 'menu:admin');

// User routes
$app->get('/{company}/user/{user}', 'UserController:home')->setName("company/user")->setArgument('access', 'order:write');
$app->post('/{company}/user/{user}', 'UserController:home')->setArgument('access', 'order:write');
$app->get('/{company}/user/{user}/order/{menu}[/{item}]', 'UserController:order')->setName("company/user/order")->setArgument('access', 'order:write');
$app->get('/{company}/user/{user}/orders', 'UserController:orders')->setName("company/user/orders")->setArgument('access', 'order:write');

//CompanyAdmin routes
//$app->get('/{company}', 'CompanyAdminController:home')->setName("company");

$app->get('/{company}/admin', 'CompanyAdminController:home')->setName("company/admin")->setArgument('access', 'company:write');
$app->post('/{company}/admin', 'CompanyAdminController:home')->setArgument('access', 'company:write');
$app->get('/{company}/admin/editOrder/{id}', 'CompanyAdminController:editOrder')->setName("company/admin/editOrder")->setArgument('access', 'company:write');
$app->post('/{company}/admin/editOrder/{id}', 'CompanyAdminController:editOrder')->setArgument('access', 'company:write');





?>