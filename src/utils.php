<?php

function makeAssoc($rows,$keyColumn){
    $result=[];
    foreach($rows as $i=>$row){
        if(!isset($row[$keyColumn]))throw new \Exception("key column '$keyColumn' not defined in row $i");
        if(isset($result[$row[$keyColumn]]))throw new \Exception("duplicated key value '".$row[$keyColumn]."'");
        $result[$row[$keyColumn]]=$row;
    }
    return $result;
}



function openDirHandle($path){
    if(!is_dir($path)) throw new Exception("Failed to open '$path' - no such directory");
    if(!is_readable($path)) throw new Exception("Failed to open '$path' directory - no 'read' permission");
//    if(!is_executable($path)) throw new Exception("Failed to open '$path' directory - no 'execute' permission");
    $d = @dir($path);
    if(!$d){
        throw new Exception("Failed to open '$path' directory");
    }
    return $d;
}

/**
 * Retrives local file list form given path
 *
 * @param string $path
 * @return array
 */
function getFiles($path,$subdir_separator=false,$prefix=''){
    $d=openDirHandle($path);
    $result=array();
    while (false !== ($entry = $d->read()))
        if(!is_dir($path.'/'.$entry)){
            $result[]=$prefix.$entry;
    }elseif($subdir_separator && $entry!='.' && $entry!='..'){
        $result=array_merge($result,getFiles($path.'/'.$entry,$subdir_separator,$prefix.$entry.$subdir_separator));
    }
    
    return $result;
}

/**
 * Retrives local directory list form given path
 *
 * @param string $path
 * @param string $subdir_separator
 * @param string $prefix
 * @return array
 */
function getDirs($path,$subdir_separator=false,$prefix=''){
    $d=openDirHandle($path);
    $result=array();
    while (false !== ($entry = $d->read()))
        if(is_dir($path.'/'.$entry)&&$entry!='.'&&$entry!='..'){
            $result[]=$prefix.$entry;
            if($subdir_separator)
                $result=array_merge($result,getDirs($path.'/'.$entry,$subdir_separator,$entry.$subdir_separator));
    }
    sort($result);
    return $result;
}

?>