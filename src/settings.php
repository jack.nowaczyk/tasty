<?php
$isDeveloper=$_SERVER['SERVER_NAME']=='localhost';
$isDocker=isset($_ENV['docker']);

return [
    
    'displayErrorDetails' => true, // set to false in production
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header
    'determineRouteBeforeAppMiddleware' => true, // needed by Authentication middleware
    
    'tasty' => [
        'data_path' => __DIR__ . '/../data/company/',
        'autocreate' => true,
        'data_dir' => __DIR__ . '/../data',
        'public_dir' => __DIR__ . '/public'
    ],
    
    'view' => [
        'template_path' => __DIR__ . '/../templates/',
        'cache' => __DIR__ . '/../templates_c/',
        'debug' => true
    ],
    
    // Monolog settings
    'logger' => [
        'name' => 'slim-app',
        'path' => $isDocker ? 'php://stdout' : __DIR__ . '/../logs/app.log',
        'level' => \Monolog\Logger::DEBUG
    ],
    
    'mailer' => $isDeveloper? [
        //localhost mailer
        'Mailer' => 'smtp',
        'SMTPSecure' => 'tls',
        'Host' => 'smtp.gmail.com',
        'Port' => 587,
        'SMTPAuth' => true,
        'Username' => "projekt.tasty@gmail.com",
        'Password' => "Spio2018@#$",
        //'SMTPDebug' => 3,
    ]:[
        //server mailer: use default PHP mail()
    ]
];
