<?php
namespace Tasty;

class FrontController extends AppController {
    protected $settins;
    
    public function __construct(AppModel $app, $settings){
        parent::__construct($app);
        $this->settins=$settings;
    }
    
    public function top(\Slim\Http\Request $request, $response, $args){
        return $this->view->render($response, 'top.html.twig');
    }
        
    
    public function join(\Slim\Http\Request $request, $response, $args){
        if($request->isPost()){
            $post = $request->getParsedBody();
            $email = $post['email'];
            $message = $post['message'];
            $this->app()->addMessage(['email'=>$email,'body'=>$message]);
            $this->app()->email($email, "joinConfirm.email.twig",['email'=>$email,'message'=>$message]);
            return $this->view->render($response, 'joinConfirm.html.twig');
        }
        return $this->view->render($response, 'join.html.twig');
    }
}


?>