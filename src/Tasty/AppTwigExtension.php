<?php

namespace Tasty;

class AppTwigExtension extends \Twig_Extension
{
    private $model;
    
    public function __construct(AppModel $model)
    {
        $this->model = $model;
    }
    
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('has_path', array($this->model, 'hasRouteAccess')),
            new \Twig_SimpleFunction('has_priviledge', array($this->model, 'hasPriviledge')),
            new \Twig_SimpleFunction('get_skin', array($this, 'getSkin')),
        ];
    }
    
    public function getSkin(){
        $map['Guest']='green-light';
        $map['CompanyUser']='green';
        $map['CompanyAdmin']='blue';
        $map['SuperAdmin']='red';
        $map['Developer']='black-light';
        
        $role=$this->model->getRole();
        $skin=@$map[$role];
        assert(isset($skin),"Skin not defined for role '$role'");
        if(!$skin)$skin='purple';
        return $skin;
    }

}
