<?php 
namespace Tasty;


class DeveloperModel {
    protected $baseDir;
    protected $publicDir;
    protected $srcDir;
    protected $schemaDir;
    protected $model;
    public function __construct(AppModel $model){
        $this->baseDir=realpath(__DIR__.'/../..');
        $this->publicDir=$this->baseDir.'/public';
        $this->srcDir=$this->baseDir.'/src';
        $this->schemaDir=$this->baseDir.'/public/schema';
        $this->model=$model;
    }
    
    public function getSchemaNames(){
        $names=[];
        foreach (getFiles($this->schemaDir,DIRECTORY_SEPARATOR) as $file){
            $names[]=preg_replace('/\.schema\.json$/', '', $file);
        }
        sort($names);
        return $names;
    }
    
    public function getSchema($name){
        return $this->getJsonFile($this->getSchemaFileName($name));
    }
    
    public function getSchemaJson($name){
        return $this->getFile($this->getSchemaFileName($name));
    }
    
    public function getSchemaUrl($name){
        return "schema/$name.schema.json";
    }
    
    public function setSchemaJson($name,$json){
        $data=@json_decode($json,true);
        if(!$data)throw new \Exception('invalid JSON data');
        $json=json_encode($data,JSON_PRETTY_PRINT);
        if(!@file_put_contents($this->getSchemaFileName($name), $json)){
            throw new \Exception("failed to save schema");
        }
    }
    
    public function getRoutes(){
        $routes=$this->model->router()->getRoutes();
        $parser=new \FastRoute\RouteParser\Std();
        foreach($routes as $route){
            $methods=$route->getMethods();
            if(count($methods)==6)$methods=['ALL'];
            
            $data[]=[
                'name'=>$route->getName(),
                'pattern'=>$route->getPattern(),
                'access'=>$route->getArgument('access'),
                'methods'=>implode(' ',$methods),
                'link'=>$this->routeLinkHelper($route),
            ];
            
        }
        return $data;
    }
    
    private function routeLinkHelper(\Slim\Route $route){
        $routeName=$route->getName();
        if(!$routeName)return;
        
        $parser=new \FastRoute\RouteParser\Std();
        
        $routeParams=[];
        foreach ($parser->parse($route->getPattern()) as $n=>$variant){
            foreach($variant as $param){
                if(is_string($param))continue;
                $paramName=$param[0];
                if(isset($routeParams[$paramName]))continue;
                $routeParams[$paramName]=$n?"'$paramName':#":"'$paramName':?";
            }
        }
        $link='{{ path_for(\''.$route->getName().'\'';
        if($routeParams){
            $link.=', {'.implode(', ', $routeParams).'}';
        }
        $link.=') }}';
        return $link;
    }
    
    private function getSchemaFileName($name){
        if(preg_match('/\.\./', $name))throw new \Exception('bogus schema name');
        return $this->schemaDir.DIRECTORY_SEPARATOR.$name.'.schema.json';
    }
    
    private function getJsonFile($filename){
        $json=$this->getFile($filename);
        $data=json_decode($json,true);
        if(is_null($data))throw new \Exception("failed to decode '$filename'");
       return $data;
    }
    
    private function getFile($filename){
        $data=@file_get_contents($filename);
        if($data===false)throw new \Exception("Failed to read '$filename' file");
        return $data;
    }
    
}




?>