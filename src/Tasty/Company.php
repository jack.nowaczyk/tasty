<?php
namespace Tasty;

class Company
{
    protected $name;
    protected $baseDir;
    protected $dataDir;
    protected $db;
    protected $properties;

    public function __construct($options) {
        if(!isset($options['data_path']))throw new \Exception("data_path option not set");
        $this->baseDir=$options['data_path'];
        if(!is_dir($this->baseDir))throw new \Exception("data_path dir '$this->baseDir' does not exist");
        if(!is_writable($this->baseDir))throw new \Exception("data_path dir '$this->baseDir' is not writable");
    }

    public function getName(){
        assert(isset($this->name));
        return $this->name;
    }
    
    public function setName($name,$create=false){
        if(!$create && $this->name==$name)return;
        $this->properties=null;
        $this->db=null;
        $this->name=$name;
        $this->dataDir=$this->baseDir.$this->name;
        if($create){
            $this->create();
        }
        
        if(!is_dir($this->dataDir))throw new \Exception("company does not exist",404);
    }
    
    
    public function getNames(){
        return getDirs($this->baseDir);
    }
    
    public function delete() {
        assert(isset($this->dataDir));
        unset($this->db);
        $this->deleteDir($this->dataDir);
    }

    public function db() {
        if(!$this->db){
            assert(isset($this->dataDir));
            $file=$this->dataDir.DIRECTORY_SEPARATOR.'db.sqlite';
            $this->db=new \PDO("sqlite:$file");
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); //throw exception on querry error rather than return false
        }
        return $this->db;
    }

    public function getProperties() {
        $this->readProperties();
        return $this->properties;
    }

    public function setProperties($properties) {
        $this->assertPropertyValid(array_keys($properties));
        $this->properties=array_merge($this->properties, $properties);
        $this->writeProperties();
    }

    public function getProperty($name) {
        $this->assertPropertyValid($name);
        return $this->properties[$name];
    }

    public function setProperty($name,$value) {
        $this->assertPropertyValid($name);
        $this->properties[$name]=$value;
        $this->writeProperties();
    }
    
    private function getPropertiesFile() {
        assert(isset($this->dataDir));
        return $this->dataDir.DIRECTORY_SEPARATOR.'properties.json';
    }
    
    private function assertPropertyValid($names) {
        if(!is_array($names))$names=array($names);
        $this->readProperties();
        $invalid=array_diff($names, array_keys($this->properties));
        if($invalid){
            throw new \Exception("property '".implode("', '",$invalid)."' is not registered in the default properties.json file");
        }
    }

    private function readProperties() {
        if(!isset($this->properties)){
            $baseFile=$this->baseDir.DIRECTORY_SEPARATOR.'properties.json';
            $this->properties=$this->getJsonData($baseFile);//read default values, only default values can be overriden
            $companyFile=$this->getPropertiesFile();
            if(is_file($companyFile)){//apply company specific properties
                $properties=$this->getJsonData($companyFile);
                $this->assertPropertyValid(array_keys($properties));
                $this->properties=array_merge($this->properties, $properties);
            }
        }
    }
    
    private function getJsonData($file){
        $data=@file_get_contents($file);
        if($data===false)throw new \Exception("failed to read json file");
        $properties=json_decode($data,true);
        if(!is_array($properties)){
            throw new \Exception("failed to decode json file");
        }
        return $properties;
    }

    private function writeProperties() {
        if(!is_array($this->properties))throw new \Exception('properties not set');
        if(!@file_put_contents($this->getPropertiesFile(), json_encode($this->properties,JSON_PRETTY_PRINT))){
            throw new \Exception('failed to write company properties');
        }
    }
    
    private function create() {
        if(strlen($this->name)<3)throw new \Exception("company name is too short");
        if(preg_match('/([^a-z0-9_])/i', $this->name, $res))throw new \Exception("company name contains invalid char '$res[1]'");
        if(is_dir($this->dataDir))throw new \Exception("comapny name is used");
        if(!@mkdir($this->dataDir))throw new \Exception("failed to create company");
        $key=sha1($this->dataDir.'|'.rand().'|salt:ds23fd$v&3~@UaB90Q'); // secret key used as a base for secure links generation
        $properties=['label' => $this->name, 'creation_time' => time(), 'key' => $key];
        try{
            $this->readProperties();//read default properties
            $this->setProperties($properties);
            $this->createTables();
        } catch (\Exception $e) {
            $this->delete(); //make creation atomic, clean the mess on failure
            throw $e;
        }
    }

    private function createTables() {
        $query=@file_get_contents($this->baseDir.DIRECTORY_SEPARATOR.'create.sql');
        if($query===false)throw new \Exception("failed to read data/create.sql file");
        $this->db()->exec($query);
    }

    private function deleteDir($path) {
        $entries = glob("$path/*");
        foreach ($entries as $entry) {
            is_dir($entry) ? $this->deleteDir($entry) : unlink($entry);
        }
        rmdir($path);
    }

}

