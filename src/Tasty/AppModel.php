<?php
namespace Tasty;

use PHPMailer\PHPMailer\PHPMailer;

class AppModel extends PdoObject{
    private $container;
    private $dataDir;
    private $publicDir;
    
    
    public function __construct(\Slim\Container $container, $options) {
        //parent::__construct($this->db());
        $this->container=$container;
        
        if(!isset($options['data_dir']))throw new \Exception("data_dir option not set");
        $this->dataDir=$options['data_dir'];
        $this->publicDir=$options['public_dir'];
    }
    
    //proxy to container object, enable type hinting for objects in container
    
    /**
     * @return \Slim\Views\Twig
     */
    public function view(){
        return $this->container->view;
    }
    

    /**
     * @return \Slim\Interfaces\RouterInterface
     */
    public function router(){
        return $this->container->router;
    }
    
    /**
     * @return \Zend\Permissions\Acl\AclInterface
     */
    public function acl(){
        return $this->container->acl;
    }
    
    /**
     * @return \PHPMailer\PHPMailer\PHPMailer
     */
    private function mailer(){
        /**
         * @var \PHPMailer\PHPMailer\PHPMailer $mailer
         */
        $mailer=$this->container->mailer;
        $mailer->clearAllRecipients();
        $mailer->clearAttachments();
        $mailer->clearCustomHeaders();
        $mailer->Body=$mailer->AltBody='';
        return $mailer;
    }
    
    /**
     * @return \PDO
     */
    protected function db() {
        if(!$this->db){
            assert(isset($this->dataDir));
            $file=$this->dataDir.DIRECTORY_SEPARATOR.'db.sqlite';
            $create=!is_file($file);
            $this->db=new \PDO("sqlite:$file");
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); //throw exception on querry error rather than return false
            if($create){
                $this->createTables();
            }
        }
        return $this->db;
    }
    
    public function getRole(){
        return isset($_SESSION['role'])?$_SESSION['role']:'Guest';
    }
    
    public function hasPriviledge($resource,$priviledge){
        return $this->acl()->isAllowed($this->getRole(), $resource, $priviledge);
    }
    
    public function requirePriviledge($resource,$priviledge){
        if(!$this->hasPriviledge($resource, $priviledge)){
            $role=$this->getRole();
            throw new \Exception("You do not have '$priviledge' priviledge to '$resource' (your system role is '$role').",403);
        }
    }
    
    public function hasRouteAccess($name,$require=false){
        $route=$this->router()->getNamedRoute($name);
        $access=$route->getArgument('access');
        if($access){
            @list($resource,$priviledge)=explode(':', $access,2);
            assert(strlen($priviledge),"Invalid access '.$access.' attribute defined for route ".$route->getName().' - expected format is "resource:priviledge"');
            if($require){
                $this->requirePriviledge($resource,$priviledge);
                return true;
            }
            return $this->hasPriviledge($resource,$priviledge);
        }
        return true;
    }
    
    public function email($email,$template,$data=array()){
        //prepare message properties
        $view=$this->view();
        $subject=trim($view->fetchBlock($template,'subject',$data));
        $from=trim($view->fetchBlock($template,'from',$data));
        $fromName=trim($view->fetchBlock($template,'fromName',$data));
        $body=trim($view->fetchBlock($template,'body',$data));
        $altBody=trim($view->fetchBlock($template,'altBody',$data));
        
        //validate
        assert(strpos($email,'@'),'email not set or invalid');
        assert(strlen($subject),'block "subject" not in "'.$template.'" set, add: {% block subject %}Email title{% endblock %}');
        assert(strlen($from),'block "from" not in "'.$template.'" set, add: {% block from %}name@domain.com{% endblock %}');
        assert(strpos($from,'@'),'from is invalid');
        assert(strlen($body),'block "body" not in "'.$template.'" set, add: {% block body %}Email contents{% endblock %}');
        
        //send email
        $mailer=$this->mailer();
        $mailer->setFrom($from, $fromName);
        $mailer->AddAddress($email);
        $mailer->Subject = $subject;
        $mailer->Encoding='base64';
        $mailer->Body = $body;
        if($altBody)$mailer->AltBody = $altBody;
        $mailer->IsHTML(true);
        return $mailer->send();
    }
    
    public function addMessage($message) {
        return $this->insertRow('messages', $message);
    }
    
    public function deleteMessage($id) {
        return $this->deleteRow('messages', $id);
    }
    
    public function getMessages($all=false) {
        $sql= "SELECT * FROM messages";
        if(!$all)$sql.=' WHERE state=0';
        $sql.=" ORDER BY id DESC";
        return $this->fetchAllRows($sql);
    }
    
    private function createTables() {
        $query=@file_get_contents($this->dataDir.DIRECTORY_SEPARATOR.'create.sql');
        if($query===false)throw new \Exception("failed to read create.sql file");
        $this->db()->exec($query);
    }
}

