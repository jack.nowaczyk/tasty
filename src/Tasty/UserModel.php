<?php
namespace Tasty;

class UserModel extends PdoObject {
    protected $id;
    protected $properties;
    protected $company;
    
    public function __construct(Company $company, $id) {
        parent::__construct($company->db());
        $this->id=$id;
        $this->company=$company;
        $this->getProperties();
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getCompany(){
        return $this->company;
    }
    
    public function getCompanyName(){
        return $this->company->getName();
    }
    
    public function getProperties() {
        if(!$this->properties){
            $this->properties=$this->fetchRow("SELECT * FROM users WHERE id=:id",['id'=>$this->id]);
            if(!$this->properties)throw new \Exception("Invalid user id");
        }
        return $this->properties;
    }
    
    public function setProperties($properties) {
        assert($this->id);
        $properties['id']=$this->id;
        $this->updateRow('users', $properties);
        $this->properties=$properties;
    }
    
}

