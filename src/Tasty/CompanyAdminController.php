<?php
namespace Tasty;
use \Slim\Views\Twig;

class CompanyAdminController extends AppController {
    protected $model;
    
    public function __construct(AppModel $app, CompanyAdminModel $model){
        parent::__construct($app);
        $this->model=$model;
        $companyName=$model->getCompany()->getName();
        
        if(!$app->hasPriviledge('company','admin')){
            if($_SESSION['company']!=$model->getCompany()->getName()){
                throw new \Exception("no 'write' permission to '$companyName' company",401);
            }
        }
        $this->view['home_url']=$this->pathFor('company/admin',['company'=>$companyName]);
    }
    
    public function home(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $error=null;
        if($request->isPost()){
            $post = $request->getParsedBody();
            $emails=$post['emails'];
            unset($post['emails']);
            $properties=array_merge($this->model->getCompany()->getProperties(),$post);
            if($post['password']){
                $properties['password']=password_hash($post['password'], PASSWORD_DEFAULT);
            }else{
                $properties['password']=$this->model->getCompany()->getProperty('password');
            }
            try{
                $this->model->setActiveUserEmails(preg_split("/\s*\n\s*/", trim($emails)));
                $this->model->getCompany()->setProperties($properties);
            }catch (\Exception $e){
                $error=$e->getMessage();
            }
        }else{
            $emails = implode("\n", $this->model->getActiveUserEmails());
            $properties = $this->model->getCompany()->getProperties();
        }
        return $this->view->render($response, 'companyAdmin/home.html.twig', [
            'error'=>$error,
            'emails' => $emails,
            'properties' => $properties,
            'menus_0' => $this->model->getMenus(0),
            'menus_1' => $this->model->getMenusReport(1),
            'menus_2' => $this->model->getMenusReport(2),
            'company' => $this->model->getCompany(),
            'users' => $this->model->getUsers(),
            'title' => $this->model->getCompany()->getProperty('label'),
            'key'=> $this->model->getSecureKey(0),
            'admin' => $this->app()->hasPriviledge('company', 'admin')
        ]);
    }
    
    public function editMenu(\Slim\Http\Request $request, \Slim\Http\Response $response, $args){
        $id=$args['id'];
        $error=null;
        if($request->isPost()){
            $menuJson = $request->getParsedBodyParam('menu');
            try{
                $menu=json_decode($menuJson,true);
                if(!$menu)throw new \Exception('invalid menu data');
                $menu['id']=$id;
                $id=$this->model->setMenu($menu);
                return $response->withRedirect($this->uriFor($request,'company/admin',$args));
            }catch (\Exception $e){
                $error=$e->getMessage();
            }
        }else{
            $menuJson=json_encode($this->model->getMenu($id));
        }

        return $this->view->render($response, 'companyAdmin/editMenu.html.twig', [
            'error'=>$error,
            'menu' => $menuJson,
            'company' => $this->model->getCompany(),
        ]);
    }
    
    public function deleteMenu(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $this->model->deleteMenu($args['id']);
        return $response->withRedirect($this->uriFor($request,'company/admin',$args));
    }
    
    
    public function editOrder(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args) {
        $id=$args['id'];
        $menu=$this->model->getMenu($id);
        if($request->isPost()){
            $post = $request->getParsedBody();
            try{
                if(!$menu)throw new \Exception('invalid menu id',404);
                if($menu['state']!=1)throw new \Exception("voting closed");
                $this->model->setOrder($id, $post['id_user'], $post['id_menu_item']);
            }catch (\Exception $e){
                $this->view['error']=$e->getMessage();
                throw $e;
            }
        }
        
        $menuItems=makeAssoc($this->model->getMenuItems($id),'id');
        $orders=makeAssoc($this->model->getOrders($id), 'id_user');
        $users=[];
        $total=0;
        foreach ($this->model->getUsers() as $user){
            $orderedItem=@$orders[$user['id']]['id_menu_item'];
            if(!$user['active'] && !$orderedItem)continue;//skip inactive only if no orders from them
            $users[]=$user;
            if($orderedItem){
                $total+=$menuItems[$orderedItem]['price'];
                @$menuItems[$orderedItem]['orders']++;
            }
        }
        
        return $this->view->render($response, 'companyAdmin/editOrder.html.twig', [
            'menu' => $menu,
            'menu_items'=>$menuItems,
            'orders'=>$orders,
            'users'=>$users,
            'company' => $this->model->getCompany(),
            'total' => $total,
            'editable' => $menu['state']==1,
        ]);
    }
    
    public function activate(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args) {
        $email=$this->model->getCompany()->getProperty('email');
        $this->app()->email($email, "companyAdmin/activate.email.twig", [
            'company'=>$this->model->getCompany(),
            'key'=> $this->model->getSecureKey(0),
        ]);
        $this->model->getCompany()->setProperty('active', true);
        return $response->withRedirect($this->uriFor($request,'company/admin',$args));
    }
    
    
}


?>