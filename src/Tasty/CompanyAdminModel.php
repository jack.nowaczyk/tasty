<?php 
namespace Tasty;


class CompanyAdminModel extends PdoObject {
    protected $company;
    protected $app;
    public function __construct(AppModel $app, Company $company){
        parent::__construct($company->db());
        $this->company=$company;
        $this->app=$app;
    }
    
    public function getCompany(){
        return $this->company;
    }
    
    public function loginUser($userID){
        if($userID){
            $user=$this->getUserById($userID);
            if(!$user)throw new \Exception("login failed, invalid user_id $userID");
        }
        $_SESSION=['user'=>$userID,'company'=>$this->company->getName(),'role'=>$userID?'CompanyUser':'CompanyAdmin'];
    }
    
    public function getCurrentUser(){
        if($this->company->getName()==@$_SESSION['company']){
            return $_SESSION['user'];
        }
        return null;
    }
    
    public function getMenusReport($state){
        return $this->fetchAllRows('SELECT menus.*, COUNT(user_orders.id_menu_item) as "orders", SUM(menu_items.price) AS "total" , SUM (user_orders.paid) AS "total_paid"
                                    FROM menus JOIN menu_items ON(menu_items.id_menu=menus.id) 
                                    JOIN user_orders ON (user_orders.id_menu_item=menu_items.id)
                                    WHERE menus.state=:state
                                    GROUP BY menus.id ORDER BY menus.valid_since,menus.name',['state'=>$state]);
    }
    
    public function getMenus($state){
        return $this->fetchAllRows("SELECT * FROM menus WHERE state=:state",['state'=>$state]);
    }
    
    public function cron($time='now'){
        $now=is_integer($time)?$time:strtotime($time);
        $menus=$this->fetchAllRows("SELECT id FROM menus WHERE (valid_since<=:now AND state=0)",['now'=>date('Y-m-d H:i:s',$now)]);
        foreach($menus as $menu) {
            echo "Processing menu $menu[id] - announce\n";
            $this->announceMenu($menu['id']);
        }
        
        $menus=$this->fetchAllRows("SELECT id FROM menus WHERE (valid_until<=:now AND state=1)",['now'=>date('Y-m-d H:i:s',$now)]);
        foreach($menus as $menu) {
            echo "Processing menu $menu[id] - close voting\n";
            $this->updateRow('menus', ['id'=>$menu['id'],'state'=>2]);
        }
    }
    
    public function cancelMenuAnnoucement($id){
        $this->deleteRow('menu_items', $id,'id_menu');
        $this->updateRow('menu', ['id'=>$id,'status'=>0]);
    }
    
    public function announceMenu($id){
        $menu=$this->getMenu($id);
        if($menu['state'])return;

        $this->db->beginTransaction();
        $this->updateRow('menus', ['id'=>$id,'state'=>1]);
        foreach($menu['items'] as $i=>$item){
            $item['id_menu']=$id;
            $item['item_number']=$i+1;
            $this->insertRow('menu_items',$item);
        }
        $this->sendEmails($id, $this->getActiveUserEmails());
        $this->db->commit();
    }
    
    
    public function sendEmails($menuId,$emails) {
        $menu=$this->getMenu($menuId);
        $menuItems=$this->getMenuItems($menuId);
        foreach ($emails as $email){
            $this->sendEmail($email, $menu, $menuItems);
        }
    }
    
    public function sendEmail($email,$menu,$menuItems){
        echo "Sending $menu[name] to $email user\n";
        $user=$this->getUserByEmail($email);
        
        $this->app->email($email, "order.email.twig",[
            'menu'=>$menu,
            'menu_items'=>$menuItems,
            'key'=>$this->getSecureKey($user['id']),
            'user'=>$user,
            'company'=>$this->company,
        ]);
    }
    
    
    
    public function getMenuItems($id){
        return $this->fetchAllRows("SELECT * FROM menu_items WHERE id_menu=:id ORDER BY item_number ASC",['id'=>$id]);
    }
    
    public function deleteMenu($id){
        $this->deleteRow('menus', $id);
    }
    
    public function getMenu($id){
        $row=$this->fetchRow("SELECT * FROM menus WHERE id=:id",['id'=>$id]);
        $row['items']=json_decode($row['items'],true);
        return $row;
    }
    
    public function getUserById($id){
        return $this->fetchRow("SELECT * FROM users WHERE id=:id",['id'=>$id]);
    }

    public function getUserByEmail($email){
        return $this->fetchRow("SELECT * FROM users WHERE email=:email",['email'=>$email]);
    }
    
    public function getActiveUsers(){
        return $this->fetchAllRows("SELECT * FROM users WHERE active=1 ORDER BY email ASC");
    }
    
    public function getUsers(){
        return $this->fetchAllRows("SELECT * FROM users ORDER BY email ASC");
    }
    
    public function getSecureKey($user_id){
        return $user_id.'-'.sha1($user_id.'|'.$this->company->getProperty('key'));
    }
    
    public function checkSecureKey($key){
        $user_id=current(explode('-', $key,2));
        if($this->getSecureKey($user_id)!==$key)throw new \Exception('authentication failed - invalid secure key',403);
        return $user_id;
    }
    
    public function getActiveUserEmails() {
        $rows= $this->fetchAllRows("SELECT email FROM users WHERE active=1 ORDER BY email ASC");
        $emails=[];
        foreach($rows as $row){
            $emails[]=$row['email'];
        }
        return $emails;
    }
    
    public function setActiveUserEmails($emails){
        $this->db->query("UPDATE users SET active=0");
        foreach($emails as $email){
            if(!$email)continue;
            $data=['email'=>$email,'active'=>1];
            $this->insertOrUpdate('users', $data, 'email');
        }
    }
    
    public function getOrders($id_menu){
        return $this->fetchAllRows('SELECT user_orders.* FROM menu_items 
                                    JOIN user_orders ON (user_orders.id_menu_item = menu_items.id)
                                    WHERE menu_items.id_menu=:id',['id'=>$id_menu]);
    }
    
    public function setOrder($id_menu,$id_user,$id_menu_item){
        $sql="DELETE FROM user_orders WHERE id_user=:id_user AND id_menu=:id_menu";
        $sth = $this->db->prepare($sql);
        $sth->execute(['id_user'=>$id_user, 'id_menu'=>$id_menu]);
        if($id_menu_item){
            $data=['id_user'=>$id_user, 'id_menu'=>$id_menu, 'id_menu_item'=>$id_menu_item];
            $this->insertRow('user_orders', $data);
        }
    }
 
    public function setMenu($menu) {
        if(!$menu['items'])throw new \Exception("no items defined");
        $menu['items']=json_encode($menu['items'],true);
        
        $menu=array_map('trim', $menu);

        if(strlen($menu['name'])<3)throw new \Exception("name is too short");
        $valid_since=strtotime($menu['valid_since']);
        $valid_until=strtotime($menu['valid_until']);
        if(!$valid_since)throw new \Exception("invalid valid_since value '$menu[valid_since]'");
        if(!$valid_until)throw new \Exception("invalid valid_until value '$menu[valid_since]'");
        $menu['valid_since']=date('Y-m-d H:i:s',$valid_since);
        $menu['valid_until']=date('Y-m-d H:i:s',$valid_until);
        
        if($menu['id']){
            $current=$this->getMenu($menu['id']);
            if(!$current)throw new \Exception("cannot update menu - invalid id");
            if($current['state'])throw new \Exception("cannot update menu - already announced");
            return $this->updateRow('menus', $menu);
        }
        unset($menu['id']);
        return $this->insertRow('menus', $menu);
    }
    
    
    public function menuOrderCount($id){
        return current($this->fetchRow("SELECT COUNT(*) FROM menus 
                                        JOIN menu_items ON (menus.id=menu_items.id_menu)
                                        JOIN user_orders ON (menu_items.id=user_orders.id_menu_item)
                                        WHERE menus.id=:id",['id'=>$id]));
    }
    
//---------------------------    
    

    
}




?>