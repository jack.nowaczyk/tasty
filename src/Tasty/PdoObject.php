<?php
namespace Tasty;

abstract class PdoObject
{
    protected $db;
    protected function __construct(\PDO $db) {
        $this->db=$db;
    }
    
    protected function db(){
        assert($this->db);
        return $this->db;
    }
    
//    protected function db() {
//        return $this->db;
//    }
    
    protected function fetchAllRows($sql,$bind=array()){
        $sth = $this->db()->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute($bind);
        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    protected function fetchRow($sql,$bind=array()){
        $sth = $this->db()->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute($bind);
        return $sth->fetch(\PDO::FETCH_ASSOC);
    }
    
    protected function fetchOne($sql,$bind=array()){
        $row = $this->fetchRow($sql,$bind);
        return current($row);
    }
    
    
    protected function insertRow($table, $data, $orStatement=''){
        $columnNames=array_keys($data);
        $bindNames=array();
        foreach($columnNames as $name){
            $bindNames[]=":$name";
        }
        
        $sql="INSERT $orStatement INTO $table (".implode(',', $columnNames).") VALUES (".implode(',', $bindNames).")";
        $sth = $this->db()->prepare($sql);
        $sth->execute($data);
        return $this->db()->lastInsertId();
    }
    
    
    protected function updateRow($table, $data, $idColumn='id') {
        if(!isset($data[$idColumn]))throw new \Exception("$idColumn not set");
        $columnNames=array_keys($data);
        foreach($columnNames as $name){
            $set[]="$name=:$name";
        }
        
        $sql="UPDATE $table SET ".implode(',', $set)." WHERE $idColumn=:$idColumn";
        $sth = $this->db()->prepare($sql);
        $sth->execute($data);
        return $data[$idColumn];
    }
    
    protected function insertOrUpdate($table, $data, $idColumn='id'){
        if(!$this->insertRow($table, $data,'OR IGNORE')){
            $this->updateRow($table, $data,$idColumn);
        }
    }
    
    protected function deleteRow($table, $id, $idColumn='id'){
        $sql="DELETE FROM $table WHERE $idColumn=:id";
        $sth = $this->db()->prepare($sql);
        $sth->execute(['id'=>$id]);
    }
    
}

