<?php
namespace Tasty;
use \Slim\Views\Twig;

class UserController extends AppController {
    protected $model;
    
    public function __construct(AppModel $app, UserModel $model){
        parent::__construct($app);
        $this->model=$model;
        if($model->getId()!==$_SESSION['user'])$app->requirePriviledge('company', 'write');
    }
    
    public function home($request, $response, $args){
        if($request->isPost()){
            $post = $request->getParsedBody();
            $properties=$post;
            $this->model->setProperties($properties);
        }  
        
        return $this->view->render($response, 'companyUser/home.html.twig', [
            'user' => $this->model->getProperties(),
            'company' => $this->model->getCompany(),
        ]);
    }  

      
    public function order($request, $response, $args){
        $id_menu=$args['menu'];
        $id_menu_item=@$args['item'];
        $id_user=$this->model->getId();
        $user=$this->model->getProperties();
        $model=new CompanyAdminModel($this->app(), $this->model->getCompany());
        $menu=$model->getMenu($id_menu);
        
        $editable=$menu['state']==1;
        $changed=$editable && isset($id_menu_item);
        if($changed) {
            $model->setOrder($id_menu, $id_user, $id_menu_item);
        } else {
            $menuOrders=makeAssoc($model->getOrders($id_menu),'id_user');
            $id_menu_item=@$menuOrders[$id_user]['id_menu_item'];
        }

        $menuItems=makeAssoc($model->getMenuItems($id_menu),'id');
        
        return $this->view->render($response, 'order.html.twig', [
            'company' => $this->model->getCompany(),
            'user' => $user,
            'menu'=>$menu,
            'menu_items'=>$menuItems,
            'id_menu_item'=>$id_menu_item,
            'changed'=>$changed,
            'editable'=>$editable,
        ]);
    }  
    
    public function orders($request, $response, $args){
        return $this->view->render($response, 'orders.html.twig', [
            'company' => $this->model->getCompanyName(),
        ]);
    }  
    
}


?>