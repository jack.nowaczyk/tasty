<?php
namespace Tasty;

class AdminController extends AppController {
    protected $model;
    
    public function __construct(AppModel $app, AdminModel $model){
        parent::__construct($app);
        $this->model=$model;
        $this->view['home_url']=$this->pathFor('admin');
    }
    
    public function home(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        return $this->view->render($response, 'admin/home.html.twig', [
            'companies'=>$this->model->getCompanies(),
            'orders'=>$this->model->getPendingOrders(),
            'messages'=>$this->app()->getMessages(),
        ]);
    }
    
    public function createCompany(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $name=$request->getParam('name');
        $this->model->createCompany($name);
        return $response->withRedirect($this->pathFor('company/admin',['company'=>$name]));
    }
    
    public function deleteCompany(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $this->model->getCompany($args['company'])->delete();
        return $response->withRedirect($this->pathFor('admin'));
    }
    
    public function deleteMessage(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $this->app()->deleteMessage($args['id']);
        return $response->withRedirect($this->uriFor($request, 'admin'));
    }
    
    public function login(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        if($this->app()->hasPriviledge('app','admin')){
            return $response->withRedirect($this->uriFor($request, 'admin'));
        }
        
        if($request->isPost()){
            $companyName = $request->getParsedBodyParam('company');
            $password = $request->getParsedBodyParam('password');
            
            if($companyName=='admin' && $password=='admin'){
                $_SESSION['role']='SuperAdmin';
                $_SESSION['user']=0;
                return $response->withRedirect($this->uriFor($request, 'admin'));
            }
            if($companyName=='developer' && $password=='developer'){
                $_SESSION['role']='Developer';
                $_SESSION['user']=0;
                return $response->withRedirect($this->uriFor($request, 'developer'));
            }
            
            try{
                $company=$this->model->getCompany($companyName);
                if(password_verify($password,$company->getProperty('password'))){
                    $model=new CompanyAdminModel($this->app(),$company);
                    $model->loginUser(0);
                    return $response->withRedirect($this->uriFor($request, 'company/admin',['company'=>$companyName]));
                }
                throw new \Exception("Bad password");
            } catch (\Exception $e){
                $this->view->error=$e->getMessage();
            }
        }
        
        return $this->view->render($response, 'admin/login.html.twig', [
            'companyName'=>@$companyName,
        ]);
    }
    
    public function logout(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $_SESSION=[];
        return $response->withRedirect($this->uriFor($request, '/'));
    }
    
    public function cron(\Slim\Http\Request $request, \Slim\Http\Response $response,array $args){
        $now=isset($_GET['now'])?strtotime($_GET['now']):time();
        echo "Starting cron (".date('Y-m-d H:i:s',$now).")\n\n";
        $t=microtime(true);
        echo "Processing companies:\n";
        foreach($this->model->getCompanies() as $company) {
            echo "Processing company ".$company->getName().":\n";
            $model=new CompanyAdminModel($this->app(),$company);
            $model->cron($now);
        }
        
        echo "\n\nAll done in ".(microtime(true)-$t)."s\n";
        return $response->withHeader('Content-Type', 'text/plain');
    }
}


?>