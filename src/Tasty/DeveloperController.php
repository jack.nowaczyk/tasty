<?php
namespace Tasty;
use \Slim\Views\Twig;

class DeveloperController extends AppController {
    protected $model;
    
    public function __construct(AppModel $app, DeveloperModel $model) {
        parent::__construct($app);
        $this->model=$model;
    }
    
    public function demo(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
        var_dump($args);
        
    }
    
    public function home(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
        return $this->view->render($response, 'developer/home.html.twig', [
            'title' => "Developer - home",
        ]);
    }
    
    public function routes(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
        return $this->view->render($response, 'developer/routes.html.twig', [
            'title' => "Developer - routes",
            'routes'=>$this->model->getRoutes(),
        ]);
    }
    
    public function schemas(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
        $schemas=[];
        foreach($this->model->getSchemaNames() as $name){
            $schemas[$name]=$this->model->getSchema($name);
            $schemas[$name]['url']=$this->model->getSchemaUrl($name);
        }
        
        return $this->view->render($response, 'developer/schemas.html.twig', [
            'title' => "Developer - schemas",
            'schemas'=>$schemas,
        ]);
    }
    
    
    public function editSchema(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args){
        $name=$args['name'];
        
        if($request->isPost()){
            $json=$request->getParam('schema');
            $this->model->setSchemaJson($name, $json);
 //           return $response->withRedirect('developer');
        }else{
            $json=$this->model->getSchemaJson($name);
        }
        
        return $this->view->render($response, 'developer/editSchema.html.twig', [
            'title' => "Developer - $name schema edit",
            'schema'=>$json,
        ]);
        
        
    }
    
}

?>