<?php 
namespace Tasty;


class AdminModel {
    protected $company;
    public function __construct($settings, Company $company){
        $this->company=$company;
    }
    
    public function getCompanies(){
        $names=$this->company->getNames();
        $companies=[];
        foreach ($names as $name){
            $company=clone $this->company;
            $company->setName($name);
            $companies[]=$company;
        }
        return $companies;
    }
    
    public function getCompany($name){
        $this->company->setName($name);
        return $this->company;
    }
    
    public function createCompany($name){
        $this->company->setName($name,true);
        return $this->company;
    }
    
    public function getPendingOrders(){
        return [];
    }
}




?>