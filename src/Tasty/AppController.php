<?php
 namespace Tasty;

 abstract class AppController {
     private $app;
     protected $view;
     protected function __construct(AppModel $model){
         $this->view=$model->view();
         $this->app=$model;
     }
     
     protected function pathFor($name, array $data = [], array $queryParams = []){
         return $this->app->router()->pathFor($name,$data,$queryParams);
     }
     
     protected function uriFor(\Slim\Http\Request $request, $name, array $data = [], array $queryParams = []){
         $path=$this->pathFor($name, $data, $queryParams);
         return $request->getUri()->withPath($path);
     }
     
     protected function app(){
         return $this->app;
     }

}