<?php
use Zend\Permissions\Acl\Acl;

$acl = new Acl();

$acl->addRole('Guest');
$acl->addRole('CompanyUser','Guest');
$acl->addRole('CompanyAdmin','CompanyUser');
$acl->addRole('SuperAdmin','CompanyAdmin');
$acl->addRole('Developer','SuperAdmin');

$acl->addResource('app');
$acl->addResource('company');
$acl->addResource('menu');
$acl->addResource('order');

//rules: 'read','write','delete','admin'
$acl->allow('Guest', null, 'read');
$acl->allow('CompanyUser', 'order', 'write');
$acl->allow('CompanyAdmin', array('company','order','menu'), array('write'));
$acl->allow('SuperAdmin', null, array('read','write','admin'));
$acl->allow('Developer', null, array('read','write','admin','debug'));

return $acl;
?>