CREATE TABLE users (
 id INTEGER PRIMARY KEY AUTOINCREMENT,
 email VARCHAR(40) UNIQUE,
 first_name VARCHAR(15),
 last_name VARCHAR(30),
 phone VARCHAR(20),
 active INTEGER NOT NULL DEFAULT 1,
 role TEXT DEFAULT "user",
 creation_time INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE user_orders (
 id_user INTEGER NOT NULL,
 id_menu INTEGER NOT NULL,
 id_menu_item INTEGER NOT NULL,
 paid FLOAT DEFAULT 0
);

CREATE TABLE menus (
 id INTEGER PRIMARY KEY AUTOINCREMENT,
 name TEXT,
 description TEXT,
 valid_since INTEGER,
 valid_until INTEGER,
 items TEXT,
 state INTEGER DEFAULT 0
);

CREATE TABLE menu_items (
 id INTEGER PRIMARY KEY AUTOINCREMENT,
 id_menu INTEGER NOT NULL,
 suplier TEXT,
 name TEXT,
 description TEXT,
 item_number INTEGER, 
 price FLOAT DEFAULT 0
);

CREATE UNIQUE INDEX `menu_items_idx` ON `menu_items` (
	`id_menu`,
	`item_number`
);