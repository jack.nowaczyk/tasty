jQuery.extend(jQuery.expr[':'], { 
  "dataStartsWith" : function(el, i, p, n) {  
    var pCamel = p[3].replace(/-([a-z])/ig, function(m,$1) { return $1.toUpperCase(); });
    return Object.keys(el.dataset).some(function(i, v){
      return i.indexOf(pCamel) > -1;
    });
  }
});

//Use like:
//$('p:dataStartsWith(foo-bar)').css({color:"red"});  

//To get a list of data attributes:
//$('p:dataStartsWith(foo-bar)').each(function(i, el){
//console.log( el.dataset );
//}); 


  
//https://github.com/json-editor/json-editor
JSONEditor.defaults.options.theme = 'bootstrap3';
JSONEditor.defaults.options.iconlib = 'bootstrap3';//'fontawesome4';
JSONEditor.defaults.editors.datetime = JSONEditor.defaults.editors.string.extend({
  build: function() {
    this._super();
    
    $(this.input).daterangepicker({
    "timePicker": true,
    "timePicker24Hour": true,
    "timePickerIncrement": 15,
    "locale": {
        "format": "YYYY-MM-DD HH:mm"
        }
	});
  },
  getNumColumns: function() {
    return 4;
  }
});
 

// Initialize the JSONeditor
$.fn.jsoneditor = function(defaultOptions) {
	$.each(this,function(i,obj){
		var schemaUrl=$(obj).data('jsoneditor-schema-url');
    	if(schemaUrl){
    		$(obj).data('jsoneditor-schema-url',false);
    		$.ajax({url: schemaUrl,context: obj}).done(function(data) {
    			defaultOptions.schema=data;
    			$(this).jsoneditor(defaultOptions);
    		});
    		return;
    	}
		
	    $.each(obj.dataset, function( index, value ) {
	    	var fullName = index.replace(/([A-Z])/g, function(m,$1) { return '-'+($1.toLowerCase());});
	    	var name = fullName.replace(/^jsoneditor-/, '');
	    	if(fullName==name || name=="schema-url")return;
	    	if (typeof value != 'object'){
	    		value=JSON.parse(value);
    		}
	    	defaultOptions[name]=value;
	    });
	    
	    if(defaultOptions.schema){
	    	console.log(defaultOptions.no_additional_properties);
	    	//schema is set - disable undefined properties by default
    		if(typeof defaultOptions.disable_properties ==='undefined')defaultOptions.disable_properties= true;
    		if(typeof defaultOptions.no_additional_properties ==='undefined'){
    			defaultOptions.no_additional_properties=true;
    			defaultOptions.required_by_default=true;
    		}
    		
	    }else{
	    	defaultOptions.schema={};//fails to initialize without it
	    }
	    defaultOptions.startval=JSON.parse($(obj).text());
	    defaultOptions.form_name_root=$(obj).prop("name");

	    var wrapper=document.createElement("div");
	    $(wrapper).addClass("jsoneditor");
	    $(obj).after(wrapper);
	    $(obj).hide();
	    var editor = new JSONEditor(wrapper,defaultOptions);
	    editor.on('change',function() {
	        var errors = editor.validate();
	        $(obj).closest('form').find(':submit').prop('disabled',errors.length>0);
	      }); 
	      
	    $(obj).closest('form').on('submit', function(){
	    	editor.disable();
	    	$(obj).text(JSON.stringify(editor.getValue()));
	    });
	});
    return this;
};





$(function () {
    $('table[data-table]').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });
    
    //handle data-jsoneditor
    $('textarea:dataStartsWith(jsoneditor)').jsoneditor({
        //defaultOptions:
    	//ajax: true
    	schema: {"title":"Menu","type":"object","format":"grid","properties":{"name":{"title":"Nazwa","type":"string","description":"Nazwa menu","minLength":4},"valid_since":{"title":"Poczatek zamównienia","type":"string","description":"Data wysylki emaili","minLength":3,"format":"datetime"},"valid_until":{"title":"Koniec zamównienia","type":"string","description":"Zamkniecie zamówienia","minLength":3,"format":"datetime"},"description":{"title":"Opis","type":"string","description":"Opis menu","format":"wyswig"},"items":{"title":"Pozycje","type":"array","format":"tabs","items":{"title":"Pozycja","type":"object","properties":{"name":{"title":"Nazwa","type":"string","description":"Nazwa dania","minLength":4},"price":{"title":"Cena","type":"number","minimum":0,"default":0,"description":"Cena dania"},"suplier":{"title":"Dostawca","type":"string","description":"Nazwa dostawcy"},"description":{"title":"Opis","type":"string","description":"Opis dania"}},"format":"grid","headerTemplate":"{{self.name}}"}}}}
    });    
});
  