# Tasty application based on Slim Framework 3 Skeleton Application

This application was built using Slim framework and uses Composer as a dependency manager. 
Can be executed on any Unix or Windows host with PHP installed. Docker configuration is available as an alternative.

## To setup development environment on Window

1. Download https://windows.php.net/downloads/releases/php-7.2.6-Win32-VC15-x86.zip
   and extract it to C:/php

2. Download https://github.com/git-for-windows/git/releases/download/v2.17.1.windows.2/Git-2.17.1.2-64-bit.exe
   and install it (select bash as default shell)

3. Edit C:\Program Files (x86)\Git\Git\etc\profile.d\env.sh
   `export PATH="$HOME/bin:$PATH:/c/php"`

4. Rename C:/php/php.ini-development to php.ini and add the following lines to it (those are already there, but commented out)
```
   date.timezone = Europe/Warsaw
   extension=php_openssl.dll
   extension=php_pdo_pgsql.dll
```
5. Open "tasty" directory in explorer and from context menu run "Git Bash here"
>   welcome to CLI world
6. Configure git user, use THE SAME values that you have in gitlab.com account
```
   git config --global user.name "John Doe"
   git config --global user.email "John.Doe@foo.com"
```
7. Check that PHP is configured properly:
   `php -i |grep "Loaded Configuration"`
   should print "Loaded Configuration File => C:\php\php.ini"
8. Download dependencies (may take a while), run tests and start the server
```
   ../composer install
   ../composer test
   ../composer start
```
9. Open "http://localhost:8080" in a browser

That's it! Now go build something cool.
